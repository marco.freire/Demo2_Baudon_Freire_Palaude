#use "edge.ml" ;;

module Triangle

=
  
struct
  type triangle = {p1 : Point.point; p2 : Point.point; p3 : Point.point}

  let makeTriangle (p1:Point.point) (p2:Point.point) (p3:Point.point) =
    {p1 = p1; p2 = p2; p3 = p3}

  let makeTriangleFromEdge (p1:Point.point) (edge:Edge.edge) =
    let p2 = Edge.getFst edge
    and p3 = Edge.getScd edge
    in makeTriangle p1 p2 p3
  ;;

  let getPoints (tri:triangle) = tri.p1, tri.p2, tri.p3

  let getEdges (t:triangle) = Edge.makeEdge t.p1 t.p2,
                              Edge.makeEdge t.p2 t.p3,
                              Edge.makeEdge t.p3 t.p1

  let sameTriangle (t1:triangle) (t2:triangle) =
    (t1.p1 = t2.p1 || t1.p1 = t2.p2 || t1.p1 = t2.p3)
    && (t1.p2 = t2.p1 || t1.p2 = t2.p2 || t1.p2 = t2.p3)
    && (t1.p3 = t2.p1 || t1.p3 = t2.p2 || t1.p3 = t2.p3)
    && (t1.p1 <> t1.p2 && t1.p2 <> t1.p3 && t1.p1 <> t1.p3)
       
end
;;

module TriangleSet

=

struct
  type triangleSet = Triangle.triangle list
      
  let emptyTriangle () = ([] : triangleSet)
  let isEmptyTriangle (u:triangleSet) =
    match u with
      | [] -> true
      | _ -> false
  let carTriangle (u : triangleSet) =
    match u with
    | [] -> failwith "carTriangle"
    | x :: _ -> x
  let cdrTriangle (u : triangleSet) =
    match u with
    | [] -> failwith "cdrTriangle"
    | _ :: xs -> (xs : triangleSet)

  let consTriangle (x:Triangle.triangle) (u:triangleSet) = (x :: u : triangleSet)

  let rec iterTriangle (f:Triangle.triangle -> unit) u =
    if not(isEmptyTriangle u)
    then
      begin
        f (carTriangle u);
        iterTriangle f (cdrTriangle u)
      end

  let rec isInTriangleSet (x:Triangle.triangle) (set:triangleSet) =
    match set with [] -> false
                 | y::m -> Triangle.sameTriangle x y || isInTriangleSet x m
                             
  let rec sameTriangleSet (set1:triangleSet) (set2:triangleSet) =
    match set1 with
    | [] -> isEmptyTriangle set2
    | x::m -> match set2 with
      | [] -> false
      | y::n -> Triangle.sameTriangle x y && sameTriangleSet m n
                || isInTriangleSet x n && isInTriangleSet y m
end
;;
