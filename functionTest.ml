#use "delaunay.ml" ;;

print_string "########## AddPoint ##########\n";;
    
let addPointTest () =  
  let testPoint = makePoint 25. 10.
  and p1 = makePoint 0. 0.
  and p2 = makePoint 50. 0.
  and p3 = makePoint 50. 50.
  in
  let testTriangles = consTriangle (makeTriangle p1 p2 p3) (emptyTriangle ())
  in
  let tri1 = makeTriangle p1 p2 testPoint
  and tri2 = makeTriangle p2 p3 testPoint
  and tri3 = makeTriangle p1 p3 testPoint
  in
  if sameTriangleSet (addPoint testTriangles testPoint)
      (consTriangle tri1 (consTriangle tri2 (consTriangle tri3
                                               (emptyTriangle ()))))
  then print_string "addPointTest : success\n"
  else print_string "addPointTest : failure\n"
;;

addPointTest () ;;


print_string "########## Point set creation ##########\n";;

let testRandomPoints () =
	let points = randomPoints 1 42. 42.
	in let firstPoint = carPoint points
		in let pointX = getX firstPoint
			and pointY = getY firstPoint
			in
				if pointX >=0. && pointX <42. 
				&& pointY >=0. && pointY <42.
					then if isEmptyPoint (cdrPoint points)
							then print_string "randomPoints : success\n"
							else print_string "randomPoints : failure; 
							the number of points is uncoherent\n"
					else print_string "randomPoints : failure; 
					the point is out of bounds\n"
;;

testRandomPoints ();;

print_string "########## drawObject ##########\n";;

print_string "Unable to test drawing functions.\n";;

print_string "########## delaunay ##########\n";;

let initialTriangleTest () =
	let expectedTriangle = [{p1= {x = 0.0 ; y = 4.2}; 
							 p2= {x = 42. ; y = 0.0}; 
							 p3= {x = 0.0 ; y = 0.0}};
							{p1= {x = 0.0 ; y = 4.2}; 
							 p2= {x = 42. ; y = 0.0}; 
							 p3= {x = 42. ; y = 4.2}}]
	in
		if (initialTriangles 42. 4.2) = expectedTriangle
			then print_string "initialTriangle : success\n"
			else print_string "initialTriangle : failure\n"
;;

initialTriangleTest ();;

print_string "########## Matrix tests ##########\n"

let testCreateSquareMat () =
  let sample = Array.make_matrix 10 10 0
  and test = createSquareMat 10 0
  and areEqual = ref true in
  for i = 0 to 9 do
    for j = 0 to 9 do
      if sample.(i).(j) <> test.(i).(j) then areEqual := false
    done;
  done;

  if !areEqual then print_string "testCreateSquareMat : success\n"
  else print_string "testCreateSquareMat : failure\n"
;;

testCreateSquareMat ();;

let testGetCell () =
  let test = [|[|1;4;7|]; [|2;5;8|]; [|3;6;9|]|] in
  let b1 = ((getCell 0 1 test) = 4) 
  and b2 = ((getCell 2 1 test) = 6) in
  if b1 && b2 then print_string "testGetCell : success\n"
  else print_string "testGetCell : failure\n"
;;

testGetCell ();;

let testSetCell () =
  let test = [|[|1;4;7|]; [|2;5;8|]; [|3;6;9|]|] in
  setCell 0 1 0 test;
  setCell 2 1 0 test;
  let b1 = ((getCell 0 1 test) = 0) 
  and b2 = ((getCell 2 1 test) = 0) in
  if b1 && b2 then print_string "testSetCell : success\n"
  else print_string "testSetCell : failure\n"
;;

testSetCell ();;

let testGetSize () =
  let test = Array.make_matrix 10 10 0 in
  let bool = ((getSize test) = 10) in
  if bool then print_string "testGetSize : success\n"
  else print_string "testGetSize : failure\n"
;;

testGetSize ();;

let testExtractMat () =
  let testIn = [|[|1.;4.;7.|]; [|2.;5.;8.|]; [|3.;6.;9.|]|] in
  let testExtr = [|[|1.;7.|]; [|3.;9.|]|] in
  let extr = extractMat 1 1 testIn in
  let areEqual = ref true in 
  for i = 0 to 1 do
    for j = 0 to 1 do
      if (extr.(i).(j) <> testExtr.(i).(j)) then
        areEqual := false
    done;
  done;
  if !areEqual then print_string "testExtractMat : success\n"
  else print_string "testExtractMat : failure\n"
;;

testExtractMat ();;

let testDetMat () =
  let test = [|[|1.;2.;3.|]; [|2.;3.;1.|]; [|3.;2.;1.|]|] in
  let det = detMat test in
  if (det = -. 12.) then print_string "testDetMat : success\n"
  else print_string "testDetMat : failure\n"
;;

testDetMat ();;
	  
let testccw () =
  let ptDir0 = makePoint 0. 0. 
  and ptDir1 = makePoint 800. 0.
  and ptDir2 = makePoint 0. 600.
  and ptIndir0 = makePoint 0. 0.
  and ptIndir1 = makePoint 0. 600.
  and ptIndir2 = makePoint 800. 0. in
  let bIndir = ccw ptIndir0 ptIndir1 ptIndir2
  and bDir = ccw ptDir0 ptDir1 ptDir2 in
  if bDir
  then if bIndir
    then print_string "testccw : failure; Indirect triangle\n"
    else print_string "testccw : success\n"
  else print_string "testccw : failure; Direct triangle\n"
;;

testccw ();;

let testInCircle () =
  let pt0 = makePoint 0. 0. in
  let pt1 = makePoint 800. 0. in
  let pt2 = makePoint 0. 600. in
  let tri = makeTriangle pt0 pt1 pt2 in
  let ptExt = makePoint 800. 600. in
  let ptIns = makePoint 100. 100. in
  let bExt = inCircle tri ptExt in
  let bIns = inCircle tri ptIns in
  if bIns then
    if bExt then
      print_string "testInCircle : failure; exterior point\n"
    else print_string "testccw : success\n"
  else print_string "testInCircle : failure; interior point\n"     
;;

testInCircle ();;


	
	
	
	
	
