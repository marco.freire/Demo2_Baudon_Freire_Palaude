open List ;;

let border triangles =
  let rec borderAux triangles edges =
    if isEmptyTriangle triangles then edges
    else let tri = carTriangle triangles
      and nextTris = cdrTriangle triangles in
      let e1, e2, e3 = getEdges tri in
      let newEdges = ref edges in
      
      (* This edge appears twice, and thus doesn't belong to the border *)
      if isInEdgeList e1 edges
      then newEdges := removeEdgeFromList e1 !newEdges
            
      (* This edge has shown up only once so far,
             and could belong to the border *)
      else newEdges := consEdge e1 !newEdges;

      (* Same for the two other edges *)
      if isInEdgeList e2 edges
      then newEdges := removeEdgeFromList e2 !newEdges
      else newEdges := consEdge e2 !newEdges;

      if isInEdgeList e3 edges
      then newEdges := removeEdgeFromList e3 !newEdges
      else newEdges := consEdge e3 !newEdges;

      borderAux nextTris !newEdges
      
  in

  borderAux triangles (emptyEdge ())
;;

let getTrianglesToRemove triangles newPoint =
  let rec getRmAux triangles removeList =
    if isEmptyTriangle triangles then removeList
    else let tri = carTriangle triangles and rest = cdrTriangle triangles in

      (* If the new point is in the triangle's circle, then this triangle
         must be removed *)
      if inCircle tri newPoint then getRmAux rest (consTriangle tri removeList)

      else getRmAux rest removeList
  in
  getRmAux triangles (emptyTriangle ())
;;

let rec removeTriangles triangles trianglesToRemove =
  let rec removeTriangle triangles triangleToRemove =
    if isEmptyTriangle triangles then triangles
    else let x = carTriangle triangles and m = cdrTriangle triangles in
      if x = triangleToRemove then removeTriangle m triangleToRemove
      else consTriangle x (removeTriangle m triangleToRemove)
  in
  if isEmptyTriangle trianglesToRemove then triangles
  else let x = carTriangle trianglesToRemove
    and m = cdrTriangle trianglesToRemove in
    removeTriangles (removeTriangle triangles x) m
;;

let addPoint triangles newPoint =

  let trianglesToRemove = getTrianglesToRemove triangles newPoint in

  let trianglesToAdd = map (makeTriangleFromEdge newPoint)
      (border trianglesToRemove) in

  concat [(removeTriangles triangles trianglesToRemove); trianglesToAdd]
;;
