module Vector

=

struct
  type vector = {x : float; y : float};;
  
  let getXV vector = vector.x;;
  let getYV vector = vector.y;;
  
  let scalProd v1 v2 =
    (getXV v1) *. (getXV v2) +. (getYV v1) *. (getYV v2);;

  let pointsToVector (p1 : Point.point) (p2 : Point.point) =
    ({x = (Point.getX p2) -. (Point.getX p1);
      y = (Point.getY p2) -. (Point.getY p1)} : vector)
  ;;
  
  let orthVector vector =
  {x = getYV vector; y = -. (getXV vector)}
end
;;
