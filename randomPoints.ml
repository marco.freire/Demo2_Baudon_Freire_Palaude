let randomPoints nb maxX maxY = 
  let rec randomPointsAux nb pointsList = 
    if nb = 0
    then pointsList
    else let newPoint = makePoint (Random.float maxX) (Random.float maxY)
      in
      randomPointsAux (nb-1) (consPoint newPoint pointsList)
  in
  randomPointsAux nb (emptyPoint ())
;;
