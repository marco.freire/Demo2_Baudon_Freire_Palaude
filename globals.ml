#load "graphics.cma" ;;

open Graphics ;;

let windowWidth = 800 ;;
let windowHeight = 600 ;;

let defaultPointCount = 50 ;;

let triangleVertexColor = blue ;;
let triangleVertexSize = 3 ;;

let pointColor = red ;;
let pointSize = 5 ;;

let lineColor = black ;;
let lineWidth = 1 ;;
