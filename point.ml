module Point

=

struct
  type point = {x : float; y : float}

  let getX (p:point) = p.x
  let getY (p:point) = p.y
  
  let makePoint (x:float) (y:float) = {x = x; y = y}

  let equalsPoint p1 p2 =
    ((getX p1) = (getX p2)) &&
    ((getY p1) = (getY p2))
end
;;

module PointSet
    
=

struct
  type pointSet = Point.point list
      
  let emptyPoint () = ([] : pointSet)
  let isEmptyPoint (u : pointSet) =
    match u with
      | [] -> true
      | _ -> false
  let carPoint (u : pointSet) =
    match u with
    | [] -> failwith "car"
    | x :: _ -> x
  let cdrPoint (u : pointSet) =
    match u with
    | [] -> failwith "cdr"
    | _ :: xs -> (xs : pointSet)

  let consPoint (x : Point.point) (u : pointSet) = (x :: u : pointSet)

  let rec iterPoint (f : Point.point -> unit) u =
    if isEmptyPoint u then ()
    else
      (
        f (carPoint u);
        iterPoint f (cdrPoint u)
      )
      
  let rec removePointFromList x l=
    if isEmptyPoint l then l
    else let y = carPoint l and m = cdrPoint l in
      if Point.equalsPoint x y then removePointFromList x m
      else consPoint y (removePointFromList x m)
end
;;
