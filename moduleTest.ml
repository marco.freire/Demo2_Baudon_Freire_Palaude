#use "delaunay.ml";;

print_string "########## point ##########\n";;

let pointTests () = 
	let testPoint = {x = 4.2 ; y = 42.0}
	in 
		if (getX testPoint) = 4.2
			then print_string "getX : success\n"
			else print_string "getX : failure\n";
		if (getY testPoint) = 42.0
			then print_string "getY : success\n"
			else print_string "getY : failure\n";
		let otherPoint = makePoint 4.2 42.0 in
			if  testPoint = otherPoint
				then print_string "makePoint : success\n"
				else print_string "makePoint : failure\n"
;;

pointTests ();;

print_string "########## pointSet ##########\n";;


let pointSetTests () =
	let point1 = {x = 4.2 ; y = 42.0}
	and point2 = {x = 0. ; y = 0.} 
	and point3 = {x = 42.0 ; y =4.2}
	and iterCount = ref 0
	in
		let testPointSet = [point1 ; point2]
		and iterfunction point = iterCount := (!iterCount) + 1
		in
			if isEmptyPoint testPointSet
				then print_string "isEmptyPoint : failure\n"
				else print_string "isEmptyPoint : success\n";
			if carPoint testPointSet = point1
				then print_string "carPoint : success\n"
				else print_string "carPoint : failure\n";
			if cdrPoint testPointSet = [point2]
				then print_string "cdrPoint : success\n"
				else print_string "cdrPoint : failure\n";
			if consPoint point3 testPointSet = [point3; point1; point2]
				then print_string "consPoint : success\n"
				else print_string "consPoint : failure\n";
			iterPoint iterfunction testPointSet;
			if (!iterCount) = 2 
				then print_string "iterPoint : success\n"
				else print_string "iterPoint : failure\n"
;;

pointSetTests ();;

print_string "########## edge ##########\n";;

let edgeTests () = 
	let point1 = {x = 4.2 ; y = 42.0}
	and point2 = {x = 0. ; y = 0.} 
	in let edge1 = {p1 = point1 ; p2 = point2}
		and edge2 = {p1 = point2 ; p2 = point1}
		in
			if (getFst edge1)= point1
				then print_string "getFst : success\n"
				else print_string "getFst : failure\n";
			if (getScd edge1)= point2
				then print_string "getScd : success\n"
				else print_string "getScd : failure\n";
			if (point1, point2) = (getPointsFromEdge edge1)
				then print_string "getPointsFromEdge : success\n"
				else print_string "getPointsFromEdge : failure\n";
			if (makeEdge point1 point2) = edge1
				then print_string "makeEdge : success\n"
				else print_string "makeEdge : failure\n";
			if equalsEdge edge1 edge2
				then print_string "equalsEdge : success\n"
				else print_string "equalEdge : failure\n"
;;

edgeTests ();;

print_string "########## edgeSet ##########\n";;

let edgeSetTests () =
	let point1 = {x = 4.2 ; y = 42.0}
	and point2 = {x = 0. ; y = 0.} 
	in let edge1 = {p1 = point1 ; p2 = point2}
		and edge2 = {p1 = point2 ; p2 = point1}
		in let edgeSetTest = [edge1; edge2]
			in
				if isEmptyEdge edgeSetTest
					then print_string "isEmptyEdge : failure\n"
					else print_string "isEmptyEdge : success\n";
				if (carEdge edgeSetTest) = edge1
					then print_string "carEdge : success\n"
					else print_string "carEdge : failure\n";
				if (cdrEdge edgeSetTest) = [edge2]
					then print_string "cdrEdge : success\n"
					else print_string "cdrEdge : failure\n";
				if (consEdge edge1 [edge2]) = edgeSetTest
					then print_string "consEdge : success\n"
					else print_string "consEdge : failure\n";
				if isEmptyEdge (removeEdgeFromList edge1 edgeSetTest) 
					then print_string "removeEdgeFromList : success\n"
					else print_string "removeEdgeFromList : failure\n";
				if isInEdgeList edge2 edgeSetTest
					then print_string "isInEdgeList : success\n"
					else print_string "isInEdgeList : failure\n"
;;

edgeSetTests ();;

print_string "########## triangle ##########\n";;

let triangleTests () =
	let point1 = {x = 4.2 ; y = 42.0}
	and point2 = {x = 0. ; y = 0.} 
	and point3 = {x = 42.0 ; y =4.2}
	in let edge1 = {p1 = point2; p2 = point3}
		and edge2 = {p1 = point3; p2 = point1}
		and edge3 = {p1 = point1; p2 = point2}
		and triangletest = {p1 = point1 ; p2 = point2 ; p3 = point3}
		in
			if (makeTriangle point1 point2 point3) = triangletest
				then print_string "makeTriangle : success\n"
				else print_string "makeTriangle : failure\n";
			if (makeTriangleFromEdge point1 edge1) = triangletest
				then print_string "makeTriangleFromEdge : success\n"
				else print_string "makeTriangleFromEdge : failure\n";
			if (point1, point2, point3) = (getPoints triangletest)
				then print_string "getPoints : success\n"
				else print_string "getPoints : failure\n";
			if (edge3, edge1, edge2) = (getEdges triangletest)
				then print_string "getEdges : success\n"
				else print_string "getEdges : failure\n"
;;

triangleTests ();;

print_string "########## triangleSet ##########\n";;

let triangleSetTests () =
	let point1 = {x = 4.2 ; y = 42.0}
	and point2 = {x = 0. ; y = 0.} 
	and point3 = {x = 42.0 ; y =4.2}
	in let triangle1 = {p1 = point1 ; p2 = point2 ; p3 = point3}
		and triangle2 = {p1 = point2 ; p2 = point1 ; p3 = point3}
		and iterCount = ref 0
		in let triangleSetTest = [triangle1; triangle2]
			and otherTriangleSet = [triangle2; triangle1]
			and iterfunction triangle = iterCount := (!iterCount) + 1
			in
				if isEmptyTriangle triangleSetTest
					then print_string "isEmptyTriangle : failure\n"
					else print_string "isEmptyTriangle : success\n";
				if carTriangle triangleSetTest = triangle1
					then print_string "carTriangle : success\n"
					else print_string "carTriangle : failure\n";
				if cdrTriangle triangleSetTest = [triangle2]
					then print_string "cdrTriangle : success\n"
					else print_string "cdrTriangle : failure\n";
				if consTriangle triangle1 [triangle2] = triangleSetTest
					then print_string "consTriangle : success\n"
					else print_string "consTriangle : failure\n";
				if sameTriangle triangle1 triangle2
					then print_string "sameTriangle : success\n"
					else print_string "sameTriangle : failure\n";
				if sameTriangleSet triangleSetTest otherTriangleSet
					then print_string "sameTriangleSet : success\n"
					else print_string "sameTriangleSet : failure\n";
				if isInTriangleSet triangle1 triangleSetTest
					then print_string "isInTriangleSet : succes\n"
					else print_string "isInTriangleSet : failure\n";
				iterTriangle iterfunction triangleSetTest;
				if (!iterCount) = 2
					then print_string "iterTriangle : success\n"
					else print_string "iterTriangle : failure\n"
;;

triangleSetTests ();;
