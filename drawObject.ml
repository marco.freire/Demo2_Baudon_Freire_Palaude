#use "globals.ml" ;;
#use "triangle.ml" ;;

let initGraphics () = 
  close_graph() ;
  open_graph (" " ^ (string_of_int windowWidth)
              ^ "x" ^ (string_of_int windowHeight) ^ "+0-0")
;;


let drawOnePoint color size fill point =
  set_color color;
  let intPointX = int_of_float (getX point)
  and intPointY = int_of_float (getY point)
  in if fill then fill_circle intPointX intPointY size
  else draw_circle intPointX intPointY size;
  set_color lineColor;
;;

let drawPoints points = 
  iterPoint (drawOnePoint pointColor pointSize false) points
;;

let drawOneEdge edge =
  let point1, point2 = getFst edge, getScd edge in
  let intPoint1X = int_of_float (getX point1)
  and intPoint1Y = int_of_float (getY point1)
  and intPoint2X = int_of_float (getX point2)
  and intPoint2Y = int_of_float (getY point2)
  in
  set_color lineColor ;
  moveto intPoint1X intPoint1Y;
  lineto intPoint2X intPoint2Y;
  drawOnePoint triangleVertexColor triangleVertexSize true point1;
  drawOnePoint triangleVertexColor triangleVertexSize true point2
;;

let drawOneTriangle triangle = 
  let point1, point2, point3 = getPoints triangle
  in
  let intPoint1X = int_of_float (getX point1)
  and intPoint1Y = int_of_float (getY point1)
  and intPoint2X = int_of_float (getX point2)
  and intPoint2Y = int_of_float (getY point2)
  and intPoint3X = int_of_float (getX point3)
  and intPoint3Y = int_of_float (getY point3)
  in
  set_color lineColor ;
  moveto intPoint1X intPoint1Y;
  lineto intPoint2X intPoint2Y;
  lineto intPoint3X intPoint3Y;
  lineto intPoint1X intPoint1Y;
  drawOnePoint triangleVertexColor triangleVertexSize true point1;
  drawOnePoint triangleVertexColor triangleVertexSize true point2;
  drawOnePoint triangleVertexColor triangleVertexSize true point3
;;

let drawTriangles triangles = 
  iterTriangle drawOneTriangle triangles
;;

let drawEverything triangles points =
  clear_graph ();
  drawTriangles triangles;
  drawPoints points
;;
