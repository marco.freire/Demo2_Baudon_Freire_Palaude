#load "graphics.cma";;
open Graphics;;

#use "triangle.ml";;
#use "globals.ml";;

open Point ;;
open Edge ;;
open Triangle ;;
open EdgeSet;;
open PointSet;;
open TriangleSet;;

#use "drawObject.ml";;
#use "matrix.ml";;
#use "triangulation.ml";;
#use "randomPoints.ml";;
