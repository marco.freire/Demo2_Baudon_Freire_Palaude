#use "include.ml" ;;

let initialTriangles maxX maxY = 
  let point00 = makePoint 0. 0.
  and point0Max = makePoint 0. maxY
  and pointMax0 = makePoint maxX 0.
  and pointMaxMax = makePoint maxX maxY
  in
  let triangle1 = makeTriangle point0Max pointMax0 point00
  and triangle2 = makeTriangle point0Max pointMax0 pointMaxMax
  in consTriangle triangle1 (consTriangle triangle2 (emptyTriangle ()))
;;

let initialDelaunay points maxX maxY =
  initGraphics () ;
  let triangles = initialTriangles maxX maxY in
  drawTriangles triangles;
  drawPoints points;
  read_key ()
;;

let w = float_of_int windowWidth and h = float_of_int windowHeight in

initialDelaunay (randomPoints defaultPointCount w h) w h;;
