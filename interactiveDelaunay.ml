(*Ajout d'un point dynamiquement*)
#use "include.ml" ;;

let initialTriangles maxX maxY =
  let point00 = {x = 0.0 ; y = 0.0}
  and point0Max = {x = 0.0 ; y = maxY}
  and pointMax0 = {x = maxX ; y = 0.0}
  and pointMaxMax = {x = maxX ; y = maxY} in
  let triangle1= {p1= point0Max; p2= pointMax0; p3= point00}
  and triangle2= {p1= point0Max; p2= pointMax0; p3= pointMaxMax} in
  [triangle1; triangle2]
;;

let interactiveDelaunay () =
  initGraphics ();
  let triangles = ref (initialTriangles (float_of_int windowWidth)
                         (float_of_int windowHeight))
  and eventList = [Button_down; Key_pressed]
  in
  let handler s =
    if s.keypressed then (* Exit when the Escape key is pressed *)
      begin if s.key = '\027' then raise Exit end
    
    else (* Add a new point and update the window after a click *)
      let newPoint = makePoint (float_of_int s.mouse_x)
          (float_of_int s.mouse_y) in
      triangles := addPoint !triangles newPoint;
      drawEverything !triangles (emptyPoint ());
  in
  loop_at_exit eventList handler
;;

interactiveDelaunay () ;;
