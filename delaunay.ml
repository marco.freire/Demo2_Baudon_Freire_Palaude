#use "include.ml" ;;

let initialTriangles maxX maxY = 
	let point00 = {x = 0.0 ; y = 0.0}
	and point0Max = {x = 0.0 ; y = maxY}
	and pointMax0 = {x = maxX ; y = 0.0}
	and pointMaxMax = {x = maxX ; y = maxY} in
		let triangle1= {p1= point0Max; p2= pointMax0; p3= point00}
		and triangle2= {p1= point0Max; p2= pointMax0; p3= pointMaxMax} in
			[triangle1; triangle2]
;;

let delaunay points maxX maxY =
  initGraphics ();
  let rec delaunayAux points triangles = 
    if isEmptyPoint points
    then drawTriangles triangles
    else let newTriangles = addPoint triangles (carPoint points)
      in delaunayAux (cdrPoint points) newTriangles
  in delaunayAux points (initialTriangles maxX maxY)
;;
	
let delaunayStepByStep points maxX maxY =
  initGraphics ();
  let rec delaunayStepByStepAux points triangles =
    if isEmptyPoint points
    then drawTriangles triangles
    else let newTriangles = addPoint triangles (carPoint points)
      and newPoints = cdrPoint points and input = ref 'a'
      in
      input := read_key ();
      clear_graph ();
      drawPoints newPoints;
      drawTriangles newTriangles;
      delaunayStepByStepAux newPoints newTriangles
  in 
  drawPoints points;
  let init = (initialTriangles maxX maxY) in
  drawTriangles init;
  delaunayStepByStepAux points init
;;

