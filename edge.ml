#use "point.ml";;

module Edge

=

struct
  type edge = {p1: Point.point; p2: Point.point}
  let getFst edge = edge.p1
  let getScd edge = edge.p2
  let getPointsFromEdge (e : edge) = getFst e, getScd e
  let makeEdge (pt1:Point.point) (pt2:Point.point) = {p1 = pt1; p2 = pt2}                   
  let equalsEdge (e1:edge) (e2:edge) =
    let coord11 = getFst e1
    and coord21 = getFst e2
    and coord12 = getScd e1
    and coord22 = getScd e2
    in
    coord11 = coord21 && coord12 = coord22
    || coord12 = coord21 && coord11 = coord22
end
;;

module EdgeSet
    
=

struct
  type edgeSet = Edge.edge list
      
  let emptyEdge () = ([] : edgeSet)
                     
  let isEmptyEdge (u : edgeSet) =
    match u with
    | [] -> true
    | _ -> false
      
  let carEdge (u : edgeSet) =
    match u with
    | [] -> failwith "car"
    | x :: _ -> x
      
  let cdrEdge (u : edgeSet) =
    match u with
    | [] -> failwith "cdr"
    | _ :: xs -> (xs : edgeSet)
      
  let consEdge (x : Edge.edge) (u : edgeSet) = (x :: u : edgeSet)

  let rec removeEdgeFromList (x:Edge.edge) (l:edgeSet) =
    if isEmptyEdge l then l
    else let y = carEdge l and m = cdrEdge l in
      if Edge.equalsEdge x y then removeEdgeFromList x m
      else consEdge y (removeEdgeFromList x m)
          
  let rec isInEdgeList (x:Edge.edge) (l:edgeSet) =
    if isEmptyEdge l then false
    else let y = carEdge l and m = cdrEdge l in
      Edge.equalsEdge x y || isInEdgeList x m
end
;;
